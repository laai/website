---
layout: news
icon: "bullhorn"
title: "Divulgação de temas para orientação em pesquisas"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) divulgou em seu [website pessoal](http://filipesaraiva.info/) uma lista de temas para orientação em pesquisas como TCC, atividades complementares, e outros. Visite a aba "Pesquisas" no [site](http://filipesaraiva.info/).
