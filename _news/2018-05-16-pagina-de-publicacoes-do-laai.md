---
layout: news
icon: "newspaper-o"
title: "Página de publicações do LAAI"
---

Agora o LAAI conta com uma [página de publicações](../publications/) para listar os artigos e demais trabalhos realizados pelo grupo. Acesse-a e fique por dentro sobre o que desenvolvemos em nossas pesquisas.
