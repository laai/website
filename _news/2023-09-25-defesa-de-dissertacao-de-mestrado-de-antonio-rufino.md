---
layout: news
icon: "graduation-cap"
title: "Defesa de dissertação de mestrado de Antonio Rufino"
---

[Antonio Rufino](../people/antonio-rufino), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá a dissertação de mestrado em Ciência da Computação intitulada "Algoritmo Augmented Randon Search como Alternativa a Algoritmos Baseados em Redes Neurais no Controle de Topologia Ativa em Sistemas de Potência". A sessão ocorrerá em 27/10/2023 às 14:00 horas via [vídeo conferência](https://conferenciaweb.rnp.br/ufpa/laai).
