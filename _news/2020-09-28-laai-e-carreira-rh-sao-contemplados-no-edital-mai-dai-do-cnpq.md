---
layout: news
icon: "dollar"
title: "LAAI e Carreira RH são contemplados no edital MAI/DAI do CNPq"
---

O LAAI e a [Carreira RH](https://carreirarh.com/), empresa piauiense do setor de recursos humanos, tiveram um projeto de pesquisa contemplado no edital [MAI/DAI 2020](http://memoria2.cnpq.br/web/guest/chamadas-publicas?p_p_id=resultadosportlet_WAR_resultadoscnpqportlet_INSTANCE_0ZaM&filtro=resultados&detalha=chamadaDivulgada&idDivulgacao=9602) do [CNPq](https://www.gov.br/cnpq/pt-br), que apoia parcerias entre empresas e universidades. O projeto sob responsabilidade do Prof. [Filipe Saraiva](../people/filipe-saraiva) aplicará inteligência computacional ao processos seletivos em recursos humanos. O edital prevê a alocação de bolsas de iniciação científica e mestrado.
