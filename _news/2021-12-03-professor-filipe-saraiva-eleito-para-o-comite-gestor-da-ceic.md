---
layout: news
icon: "university"
title: "Professor Filipe Saraiva eleito para o Comitê Gestor da CEIC"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva/) foi eleito para compôr o Comitê Gestor da CEIC - [Comissão Especial de Inteligência Computacional](https://www.sbc.org.br/14-comissoes/102-inteligencia-computacional) da SBC - [Sociedade Brasileia de Computação](https://www.sbc.org.br/14-comissoes/102-inteligencia-computacional), em um mandato com duração inicial de 2 anos.
