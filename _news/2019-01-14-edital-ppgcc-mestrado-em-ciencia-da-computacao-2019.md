---
layout: news
icon: "bullhorn"
title: "Edital PPGCC mestrado em ciência da computação 2019"
---

O PPGCC divulgou o [edital 2019](http://www.ppgcc.propesp.ufpa.br/index.php/br/ingresso/selecao-atual) para o mestrado em ciência da computação. O Prof. [Filipe Saraiva](../people/filipe-saraiva) disponibilizou 2 vagas no edital para o [projeto de pesquisa](../research/metodos-metaheuristicos-e-inteligencia-computacional-para-otimizacao-modelagem-e-simulacoes-de-sistemas-eletricos-de-potencia-e-redes-eletricas-inteligentes/) sobre metaheurísticas ou sistemas multiagentes para _smart grids_ em sistemas de distribuição. O professor também escreveu um [post](http://blog.filipesaraiva.info/?p=2053) com mais detalhes sobre o tipo de pesquisa e o perfil de alunos que ele busca.
