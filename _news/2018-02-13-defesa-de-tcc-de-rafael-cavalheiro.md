---
layout: news
icon: "graduation-cap"
title: "Defesa de TCC de Rafael Cavalheiro"
---

O aluno [Rafael Cavalheiro](../people/rafael-cavalheiro), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá seu TCC em Bacharelado em Sistemas de Informação no dia 21/02/2018 às 10:30 na sala FC-02 da Faculdade de Computação. O título do trabalho é "Sistema de Informação Integrado a Smart Meter construído com Arduino".
