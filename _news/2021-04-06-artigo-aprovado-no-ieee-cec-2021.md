---
layout: news
icon: "file-text"
title: "Artigo aprovado no IEEE CEC'2021"
---

O aluno de mestrado [Italo Campos](../people/italo-campos), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), teve o artigo intitulado "_A Reactive Multiagent System for Self-healing in Smart Distribution Grids_" aprovado para publicação no [IEEE CEC'2021](https://cec2021.mini.pw.edu.pl/). Parabéns aos autores!
