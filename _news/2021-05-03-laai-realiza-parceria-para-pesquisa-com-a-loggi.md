---
layout: news
icon: "dollar"
title: "LAAI realiza parceria para pesquisa com a Loggi"
---

O LAAI e a [Loggi](https://www.loggi.com/), empresa nacional do setor de logística, formalizaram uma parceria para desenvolvimento de pesquisas relacionadas a aplicação de métodos inteligentes ao problema de roteamento de veículos. O projeto está sob responsabilidade do Prof. [Filipe Saraiva](../people/filipe-saraiva) e se traduzirá em bolsas de iniciação científica e mestrado.
