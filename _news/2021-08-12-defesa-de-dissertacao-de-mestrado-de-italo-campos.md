---
layout: news
icon: "graduation-cap"
title: "Defesa de dissertação de mestrado de Italo Campos"
---

O aluno [Italo Campos](../people/italo-campos), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá a dissertação de mestrado em Ciência da Computação intitulada "Sistema Multiagente Reativo Aplicado à Autorrecuperação de Smart Grids com Balanceamento de Tensão". A sessão ocorrerá em 30/08/2021 às 9:00 horas via [vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai).
