---
layout: news
icon: "slideshare"
title: "I WCRIA - Workshop em Criatividade, Inovação e Inteligência Artificial"
---

O LAAI está organizando, entre os dias 22, 23, 24 e 29 de agosto, o [I WCRIA - Workshop em Criatividade, Inovação e Inteligência Artificial](https://www.aedi.ufpa.br/criar/index.html). O evento conta com palestras sobre temas desenvolvidos no laboratório além de um concurso de ideias e uma mostra de projetos. Maiores informações no site do evento.
