---
layout: news
icon: "slideshare"
title: "Apresentação: \"Padrões de Projetos para Organizações de Contratos Inteligentes Baseados em Blockchain\""
---

No próximo dia 02/07 às 10:00h o aluno [Alan Veloso](../people/alan-veloso) fará uma apresentação sobre seu projeto de pesquisa intitulado "Padrões de Projetos para Organizações de Contratos Inteligentes Baseados em Blockchain". O evento será realizado na [sala de vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai) do LAAI.
