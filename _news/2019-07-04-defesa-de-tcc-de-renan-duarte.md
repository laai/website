---
layout: news
icon: "graduation-cap"
title: "Defesa de TCC de Renan Duarte"
---

O aluno [Renan Duarte](../people/renan-duarte), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá seu TCC em Bacharelado em Ciência da Computação no dia 11/07/2019 às 10:30 horas no Labcomp-01 da Faculdade de Computação/ICEN. O título do trabalho é "Comparação de operadores de cruzamentos utilizados em Algoritmos Genéticos aplicados aos Problemas da Cobertura de Conjuntos e da Mochila Multidimensional".
