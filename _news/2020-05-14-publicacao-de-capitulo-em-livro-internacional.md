---
layout: news
icon: "file-text"
title: "Publicação em capítulo de livro internacional"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) em parceria com os professores Eduardo Asada e João London Júnior, ambos da Universidade de São Paulo, tiveram o texto "[_Distribution Systems - Network Reconfiguration_](https://doi.org/10.1002/9781119602286.ch5)" publicado em capítulo do livro "[_Applications of Modern Heuristic Optimization Methods in Power and Energy Systems_](https://doi.org/10.1002/9781119602286)", organizado por Kwang Lee e Zita Vale e publicado pela editora Willey na série IEEE Press. Parabéns aos autores!
