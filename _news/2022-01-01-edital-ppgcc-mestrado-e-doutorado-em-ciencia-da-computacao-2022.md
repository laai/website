---
layout: news
icon: "bullhorn"
title: "Edital PPGCC Mestrado e Doutorado em Ciência da Computação 2022"
---

O PPGCC divulgou os [editais 2022](http://www.ppgcc.propesp.ufpa.br/index.php/br/ingresso/selecao-atual) para o Mestrado e Doutorado em Ciência da Computação. Para o edital de mestrado, o Prof. [Filipe Saraiva](../people/filipe-saraiva) disponibilizou 2 vagas enquanto o Prof. [Lídio Campos](../people/lidio-campos) tem 1 vaga. Para o doutorado, o processo é feito em fluxo contínuo com limite de 15 vagas para todo o programa. Confira a [página dos editais](http://www.ppgcc.propesp.ufpa.br/index.php/br/ingresso/selecao-atual) para maiores detalhes, requisitos e datas importantes.
