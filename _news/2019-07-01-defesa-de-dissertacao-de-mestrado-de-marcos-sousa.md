---
layout: news
icon: "graduation-cap"
title: "Defesa de dissertação de mestrado de Marcos Sousa"
---

O aluno [Marcos Sousa](../people/marcos-sousa), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá sua dissertação de mestrado em Ciência da Computação no dia 08/07/2019 às 10:00 horas no Labcomp-01 da Faculdade de Computação/ICEN. O título do trabalho é "Agente Fuzzy para Redução da Fatura de Energia Elétrica em Cenários de Geração Distribuída".
