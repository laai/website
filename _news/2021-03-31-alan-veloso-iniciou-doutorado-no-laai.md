---
layout: news
icon: "users"
title: "Alan Veloso iniciou doutorado no LAAI"
---

[Alan Veloso](../people/alan-veloso), que recentemente defendeu dissertação de mestrado, deu continuidade aos estudos iniciando o doutorado sob orientação do Prof. Antônio Abelém e co-orientação do Prof. [Filipe Saraiva](../people/filipe-saraiva). Alan pretende continuar os estudos na intersecção entre blockchain e aprendizado de máquina. Bom trabalho, Alan!
