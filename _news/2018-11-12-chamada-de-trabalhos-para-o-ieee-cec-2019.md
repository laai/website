---
layout: news
icon: "bullhorn"
title: "Chamada de trabalhos para o IEEE CEC'2019"
---

Está aberta a chamada de trabalhos para a sessão especial intitulada [_Evolutionary Algorithms for Complex Optimization in the Energy Domain_](http://www.gecad.isep.ipp.pt/ieee-CEC2019/CEC2019-SS/), evento que ocorrerá durante o [IEEE CEC'2019](http://cec2019.org/) em Wellington. A sessão especial é uma atividade do [_IEEE CIS Task Force on Computational Intelligence in the Energy Domain_](http://ci4energy.uni-paderborn.de/), do qual o Prof. [Filipe Saraiva](../people/filipe-saraiva) faz parte.
