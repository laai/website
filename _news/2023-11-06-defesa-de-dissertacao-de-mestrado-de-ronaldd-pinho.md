---
layout: news
icon: "graduation-cap"
title: "Defesa de dissertação de mestrado de Ronaldd Pinho"
---

[Ronaldd Pinho](../people/ronaldd-pinho), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá a dissertação de mestrado em Ciência da Computação intitulada "Operador de Cruzamento Guloso para o Problema de Roteamento de Veículos com Capacidade". A sessão ocorrerá em 30/11/2023 às 09:30 na [sala de vídeo conferência](https://conferenciaweb.rnp.br/ufpa/laai) do LAAI.
