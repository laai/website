---
layout: news
icon: "bullhorn"
title: "Chamada para bolsistas PIBIC e voluntários"
---

Está [aberta chamada](../docs/chamada-filipe-bolsistas2017.pdf) para preenchimento de uma vaga para bolsista PIBIC e mais algumas vagas de iniciação científica voluntária para projeto de pesquisa coordenado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva) sobre aplicações de métodos de inteligência computacional a problemas de sistemas elétricos de distribuição.
