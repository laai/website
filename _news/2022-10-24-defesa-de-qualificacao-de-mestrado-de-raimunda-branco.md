---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Mestrado da Raimunda Branco"
---

[Raimunda Branco](../people/raimunda-branco) fará seu exame de qualificação de mestrado em 8 de novembro de 2022 às 9:30h em sala do ICEN. O título do trabalho é "_Sistema Fuzzy para Otimizar a Utilização de Energia Elétrica Solar Fotovoltaica em um Cenário de Tarifa Branca em Unidades Residenciais_".
