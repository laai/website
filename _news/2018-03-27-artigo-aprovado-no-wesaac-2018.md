---
layout: news
icon: "file-text"
title: "Artigo aprovado no WESAAC'2018"
---

[Italo Campos](../people/italo-campos) teve artigo sobre autorrecuperação em sistemas elétricos de distribuição utilizando sistemas multiagentes aprovado no [WESAAC'2018](http://uece.wesaac.com/). Parabéns!
