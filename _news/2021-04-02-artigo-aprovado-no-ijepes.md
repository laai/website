---
layout: news
icon: "file-text"
title: "Artigo aprovado no IJEPES"
---

A aluna de doutorado [Jeanne Pereira](../people/jeanne-pereira), orientada pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), teve o artigo intitulado "_Convolutional Neural Network Applied to Detect Electricity Theft: A Comparative Study on Unbalanced Data Handling Techniques_" aprovado para publicação no periódico "[_International Journal of Electrical Power & Energy Systems_](https://www.journals.elsevier.com/international-journal-of-electrical-power-and-energy-systems)", importante revista científica da área de Engenharia Elétrica. Parabéns aos autores!
