---
layout: news
icon: "slideshare"
title: "Palestra sobre geração distribuída e sistemas fuzzy no PPGCA-UEPA"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) realizará a palestra "Apontamentos sobre Geração Distribuída de Energia com Aplicação de Inteligência Computacional", baseada nos trabalhos do aluno [Marcos Sousa](../people/marcos-sousa), para membros do [Programa de Pós-Graduação em Ciência Ambiental da UEPA](https://paginas.uepa.br/pcambientais/). A apresentação acontecerá em 29/05/2019 e os slides podem ser acessados [neste link](https://speakerdeck.com/filipesaraiva/apontamentos-sobre-geracao-distribuida-de-energia-com-aplicacao-de-inteligencia-computacional).
