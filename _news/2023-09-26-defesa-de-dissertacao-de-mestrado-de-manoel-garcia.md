---
layout: news
icon: "graduation-cap"
title: "Defesa de dissertação de mestrado de Manoel Garcia"
---

[Manoel Garcia](../people/manoel-garcia), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá a dissertação de mestrado em Ciência da Computação intitulada "Uso de _Word Embeddings_ na Língua Portuguesa em Tarefas de _E-Recruiting_". A sessão ocorrerá em 30/10/2023 às 9:30 horas via [vídeo conferência](https://conferenciaweb.rnp.br/ufpa/laai).
