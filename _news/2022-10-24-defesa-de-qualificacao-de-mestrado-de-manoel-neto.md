---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Mestrado da Manoel Neto"
---

[Manoel Neto](../people/manoel-neto) fará seu exame de qualificação de mestrado em 4 de novembro de 2022 às 9:00h na sala de [webconferência do LAAI](https://conferenciaweb.rnp.br/webconf/laai). O título do trabalho é "_Uso de Word Embeddings na Língua Portuguesa em Tarefas de E-Recruiting_".
