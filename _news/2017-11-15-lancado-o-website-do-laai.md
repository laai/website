---
layout: news
icon: "globe"
title: "Lançado o website do LAAI"
---

O site do LAAI está no ar! No momento ainda em fase de construção (versão beta?) mas já com algumas informações para divulgação das atividades do laboratório.
