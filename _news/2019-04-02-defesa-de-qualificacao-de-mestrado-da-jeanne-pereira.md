---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Mestrado da Jeanne Pereira"
---

[Jeanne Pereira](../people/jeanne-pereira) fará o exame de qualificação de mestrado em 15 de abril de 2019 às 10:00h, na sala FC-01 da Faculdade de Computação/ICEN. O título do trabalho é "_Utilização de Long Short Term Memory e Conditional Boltzmann Machine para Detecção de Furto ou Fraude de Consumidores de Energia Elétrica_".
