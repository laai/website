---
layout: news
icon: "slideshare"
title: "Alunos do LAAI no SEMINIC'2019 na UFPA"
---

Os alunos [Kelly Costa](../people/kelly-costa/) e [Ronaldd Pinho](../people/ronaldd-pinho/), orientados pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), apresentarão os resultados de suas pesquisas durante o [XXX Seminário de Iniciação Científica da UFPA](http://seminariopibic.ufpa.br/). A sessão acontecerá dia 01/10/2019 das 15:00 às 15:30 na sala 207 do Mirante do Rio.
