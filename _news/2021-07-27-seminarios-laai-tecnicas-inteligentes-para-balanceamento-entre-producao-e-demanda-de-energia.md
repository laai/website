---
layout: news
icon: "slideshare"
title: "Seminários LAAI: Técnicas Inteligentes para Balanceamento entre Produção e Demanda de Energia"
---

Dia 29/07 às 10:00h teremos mais uma edição do "Seminários LAAI" para divulgar as pesquisas desenvolvidas no laboratório. Nesta edição, [Jeanne Pereira](../people/jeanne-pereira) falará sobre técnicas inteligentes para balanceamento entre produção e demanda de energia elétrica, tema de seu doutorado. O seminário acontecerá na [sala de vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai) do LAAI. Confira nos links o [banner do evento](../img/jeanne-seminarios-laai-2021.png) e um [calendário](../calendar/jeanne-seminarios-laai-2021.ical) para salvar em sua agenda.
