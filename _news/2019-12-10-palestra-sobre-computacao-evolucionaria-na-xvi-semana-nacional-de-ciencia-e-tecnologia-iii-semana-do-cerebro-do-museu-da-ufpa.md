---
layout: news
icon: "slideshare"
title: "Palestra sobre computação evolucionária na XVI Semana Nacional de Ciência e Tecnologia/III Semana do Cérebro do Museu da UFPA"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) ministrará a palestra intitulada "O Evolucionismo Resolvendo Problemas de Otimização" durante a atividade III Semana do Cérebro, integrante da [XVI Semana Nacional de Ciência e Tecnologia](https://snct.mctic.gov.br/). A apresentação será realizada no dia 18/12 às 16h no Museu da UFPA. Os slides estão disponíveis [neste link](https://speakerdeck.com/filipesaraiva/o-evolucionismo-resolvendo-problemas-de-otimizacao).
