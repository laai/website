---
layout: news
icon: "graduation-cap"
title: "Defesa de TCC de Italo Campos"
---

O aluno [Italo Campos](../people/italo-campos), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá seu TCC em Bacharelado em Ciência da Computação no dia 20/12/2018 às 10:00 horas na sala FC-02 da Faculdade de Computação/ICEN. O título do trabalho é "Aplicação de Sistemas Multiagentes ao Problema de Autorrecuperação em Sistemas Elétricos de Distribuição do tipo Smart Grid".
