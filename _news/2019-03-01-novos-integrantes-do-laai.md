---
layout: news
icon: "users"
title: "Novos integrantes do LAAI"
---

O início do ano trouxe muitos novos membros para o LAAI. Em nível de mestrado temos agora mais 2 membros, [Alan Veloso](../people/alan-veloso/) e [Italo Campos](../people/italo-campos) que foi pesquisador de graduação no laboratório. No nível de graduação entram na equipe Kelly Costa e Ronaldd Pinho, pesquisando respectivamente sistemas multiagentes e algoritmos genéticos. Sejam todos bem-vindos!
