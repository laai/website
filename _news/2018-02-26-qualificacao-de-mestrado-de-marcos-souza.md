---
layout: news
icon: "graduation-cap"
title: "Qualificação de mestrado de Marcos Sousa"
---

A banca de qualificação do mestrado de [Marcos Sousa](../people/marcos-sousa), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), está agendada para o dia 21/03/2018 às 10:00 na sala FC-02 da Faculdade de Computação. O título do trabalho é "Utilização de Agentes e Inteligência Computacional para Gerenciamento e Integração de Fontes de Energia Renováveis".
