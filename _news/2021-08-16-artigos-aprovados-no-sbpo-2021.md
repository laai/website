---
layout: news
icon: "file-text"
title: "Artigos aprovados no SBPO'2021"
---

Dois artigos produzidos por pesquisadores do LAAI foram aprovados no [SBPO'2021](https://sbpo2021.galoa.com.br/): um de autoria da aluna de doutorado [Jeanne Pereira](../people/jeanne-pereira) intitulado "Análise Comparativa do Reactive GRASP usando Path-Relinking e Variable Neighborhood Search Aplicado ao Problema da Diversidade Máxima", outro de autoria do ex-aluno de graduação [Renan Duarte](../people/renan-duarte) intitulado "Análise Comparativa entre Operadores de Cruzamento Clássicos e Operador de Fusão para o Problema da Cobertura de Conjuntos". Ambos os trabalhos foram orientados pelo Prof. [Filipe Saraiva](../people/filipe-saraiva). Parabéns aos autores!
