---
layout: news
icon: "graduation-cap"
title: "Defesa de dissertação de mestrado de Alan Veloso"
---

O aluno [Alan Veloso](../people/alan-veloso), orientada pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá a dissertação de mestrado em Ciência da Computação intitulada "Padrões de Projeto para Estruturas Organizacionais de Multi-contratos Inteligentes". A sessão ocorrerá no dia 26/03/2021 às 15:00 horas via [vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai).
