---
layout: news
icon: "users"
title: "Novo Aluno de Mestrado no LAAI"
---

O aluno Pedro Cássio foi recentemente selecionado para o mestrado no PPGCC sob orientação do Prof. [Filipe Saraiva](../people/filipe-saraiva). Ele realizará pesquisas no LAAI sobre a aplicação de métodos inteligentes para a decisão sobre prospecção de poços de petróleo. Bem-vindo ao grupo!
