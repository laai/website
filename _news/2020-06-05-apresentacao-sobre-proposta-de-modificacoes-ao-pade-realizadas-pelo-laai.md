---
layout: news
icon: "slideshare"
title: "Apresentação sobre proposta de modificações ao PADE realizadas pelo LAAI"
---

No próximo dia 08/06 às 10:00h o aluno [Italo Campos](../people/italo-campos) fará uma apresentação sobre as contribuições desenvolvidas por ele ao [PADE](https://github.com/grei-ufc/pade), <i>framework</i> para desenvolvimento de sistemas multiagentes escrito em Python e mantido pelo [GREI-UFC](https://grei-ufc.github.io/). A apresentação será realizada na [sala de vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai) do LAAI.
