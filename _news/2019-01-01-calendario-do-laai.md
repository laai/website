---
layout: news
icon: "newspaper-o"
title: "Calendário do LAAI"
---

O LAAI agora conta com uma página de [calendário](../calendar) onde serão divulgadas datas importantes de reuniões, seminários, chamada de trabalhos para conferências e periódicos de interesse do laboratório, entre outros. Também é possível acompanhar o calendário utilizando softwares de agenda, bastando apenas subscrever a [URL do arquivo](https://gitlab.com/laai/website/raw/master/laai.ics).
