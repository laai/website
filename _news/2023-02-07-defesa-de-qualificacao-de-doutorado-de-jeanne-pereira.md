---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Doutorado de Jeanne Pereira"
---

[Jeanne Pereira](../people/jeanne-pereira) fará seu exame de qualificação de doutorado em 28 de feveriro de 2023 às 9:00h na [sala de webconferência do LAAI](https://conferenciaweb.rnp.br/webconf/laai). O título do trabalho é "_Aprendizado por reforço aplicado a jogos baseados em grafos multi-agente para o balanceamento de oferta e demanda de energia elétrica no contexto de geradores distribuídos_".
