---
layout: news
icon: "slideshare"
title: "Seminários LAAI: Algoritmos de Matching de Texto"
---

Dia 24/11 às 10:00h teremos mais uma edição do "Seminários LAAI" para divulgar as pesquisas desenvolvidas no laboratório. Nesta edição, [Manoel Garcia](../people/manoel-garcia/) falará sobre algoritmos de _matching_ de textos, que atualmente utiliza em sua pesquisa de mestrado. O seminário acontecerá na [sala de vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai) do LAAI.
