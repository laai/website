---
layout: news
icon: "graduation-cap"
title: "Defesa de dissertação de mestrado de Jeanne Pereira"
---

A aluna [Jeanne Pereira](../people/jeanne-pereira), orientada pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá a dissertação de mestrado em Ciência da Computação intitulada "Técnicas de Balanceamento de Dados para Métodos de Aprendizado de Máquina Aplicados à Detecção de Anomalias no Consumo de Energia Elétrica". A sessão ocorrerá no dia 05/06/2020 às 9:00 horas via [vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai).
