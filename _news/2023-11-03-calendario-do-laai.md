---
layout: news
icon: "newspaper-o"
title: "O Retorno do Calendário do LAAI"
---

Retornamos com o [calendário](../calendar) do LAAI agora que conseguimos uma maneira mais simples de atualizá-lo. Nele serão divulgadas datas importantes de reuniões, seminários, chamada de trabalhos para conferências e periódicos de interesse do laboratório, entre outros. Também é possível acompanhar o calendário utilizando softwares de agenda, bastando apenas subscrever a [URL do arquivo](https://calendar.google.com/calendar/ical/c_0721ffebc4e9722ef81ea086b5be33cd76409b1b7fcf1ac95a65a4a3d82bac44%40group.calendar.google.com/public/basic.ics).
