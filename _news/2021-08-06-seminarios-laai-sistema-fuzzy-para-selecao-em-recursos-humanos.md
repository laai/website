---
layout: news
icon: "slideshare"
title: "Seminários LAAI: Sistemas Fuzzy para Seleção em Recursos Humanos"
---

Dia 09/08 às 14:30h teremos mais uma edição do "Seminários LAAI" para divulgar as pesquisas desenvolvidas no laboratório. Nesta edição, [Manoel Garcia](../people/manoel-garcia) falará sobre sistemas fuzzy aplicados ao processo seletivo em recursos humanos, tema de seu mestrado. O seminário acontecerá na [sala de vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai) do LAAI.
