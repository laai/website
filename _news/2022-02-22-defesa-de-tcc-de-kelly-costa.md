---
layout: news
icon: "graduation-cap"
title: "Defesa de TCC de Kelly Costa"
---

A aluna [Kelly Costa](../people/kelly-costa), orientada pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá seu TCC do Bacharelado em Ciência da Computação no dia 24/02/2022 às 14:00 horas na [sala de webconferência do LAAI](https://conferenciaweb.rnp.br/webconf/laai). O título do trabalho é "_Multi-Agent System Integrated to Power Systems Analysis Tool for Self-Healing in Smart Distribution Systems_".
