---
layout: news
icon: "file-text"
title: "Artigo aprovado no BRACIS'2023"
---

[Raimunda Branco](../people/raimunda-branco), aluna de mestrado orientada pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), teve o artigo intitulado "_Improved Fuzzy Decision System for Energy Bill Reduction in the Context of the Brazilian White Tariff Scenario_" aprovado para publicação no [BRACIS'2023](https://www.bracis.dcc.ufmg.br/). Parabéns aos autores!
