---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Mestrado da Alan Veloso"
---

[Alan Veloso](../people/alan-veloso) fará o exame de qualificação de mestrado em 7 de abril de 2020 às 15:00h via webconferência. O título do trabalho é "_Arcabouço de Paradigmas Estruturais para Organizações Autônomas Descentralizadas Baseadas em Blockchain_".
