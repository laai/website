---
layout: news
icon: "file-text"
title: "Artigo aprovado no IJCNN'2021"
---

O aluno de ciência da computação [Yann Figueiredo](../people/yann-figueiredo), orientado pelo Prof. [Lídio Campos](../people/lidio-campos), teve o artigo intitulado "_Machine Learning for Wind Power Forecasting_" aprovado para publicação no [IJCNN'2021](https://www.ijcnn.org/). Parabéns aos autores!
