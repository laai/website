---
layout: news
icon: "newspaper-o"
title: "Entrevista sobre Inteligência Computacional no PPGCC"
---

Nessa terça 22/06 às 11h o Prof. [Filipe Saraiva](../people/filipe-saraiva) e o Prof. [Jefferson Morais](http://lattes.cnpq.br/5219735119295290) serão entrevistados sobre os trabalhos que veem desenvolvendo na área de Inteligência Computacional, servindo de apresentação para alunos interessados no tema. A entrevista ocorrerá [nesta sala via YouTube](https://www.youtube.com/watch?v=VRytpiFWTQI) e é uma atividade em conjunto da [FACOMP](https://computacao.ufpa.br/) e do [PPGCC](https://ppgcc.propesp.ufpa.br/).
