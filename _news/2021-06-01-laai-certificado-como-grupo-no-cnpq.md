---
layout: news
icon: "newspaper-o"
title: "LAAI certificado como grupo de pesquisa no CNPq"
---

O LAAI foi certificado como "Grupo de Pesquisa no Brasil" no Diretório de Grupos de Pesquisa do CNPq, formalizando assim as pesquisas e formações desenvolvidas no laboratório. A [página do LAAI no diretório](http://dgp.cnpq.br/dgp/espelhogrupo/736754) já está disponível para acesso público.
