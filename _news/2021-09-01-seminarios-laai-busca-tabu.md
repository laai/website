---
layout: news
icon: "slideshare"
title: "Seminários LAAI: Busca Tabu"
---

Dia 02/09 às 10:00h teremos mais uma edição do "Seminários LAAI" para divulgar as pesquisas desenvolvidas no laboratório. Nesta edição, Marco Nascimento falará sobre o método de otimização Busca Tabu, que aplicará em suas pesquisas sobre logística. O seminário acontecerá na [sala de vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai) do LAAI.
