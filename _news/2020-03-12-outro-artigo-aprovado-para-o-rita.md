---
layout: news
icon: "file-text"
title: "(Outro) Artigo aprovado para o RITA"
---

O prof. [Filipe Saraiva](../people/filipe-saraiva), juntamente com uma equipe de pesquisadores e professores do [GREI-UFC](https://github.com/grei-ufc/), tiveram o artigo intitulado "_Mosaik and PADE: Multiagents and Co-simulation for smart grids modeling_" aprovado para publicação no periódico [RITA - Revista de Informática Teórica e Aplicada](https://seer.ufrgs.br/rita/). Essa publicação é motivada pelo artigo ter sido premiado como um dos melhores do WESAAC'2019. Parabéns!
