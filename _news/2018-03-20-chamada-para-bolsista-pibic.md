---
layout: news
icon: "bullhorn"
title: "Chamada para bolsista PIBIC"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) [abriu chamada](../docs/chamada-filipe-bolsistas2017-2.pdf) para bolsista PIBIC no projeto de pesquisa sobre aplicações de métodos de inteligência computacional a problemas de sistemas elétricos de distribuição.
