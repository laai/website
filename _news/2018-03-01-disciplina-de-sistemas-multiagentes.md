---
layout: news
icon: "pencil-square"
title: "Disciplina de sistemas multiagentes"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) ministrará a disciplina de Sistemas Multiagentes durante o período 2018.1 no [PPGCC](http://ppgcc.propesp.ufpa.br/). O horário da disciplina será das 14:50 às 16:30 nas terças e quintas.
