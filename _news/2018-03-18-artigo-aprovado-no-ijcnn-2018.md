---
layout: news
icon: "file-text"
title: "Artigo aprovado no IJCNN'2018"
---

O artigo intitulado "_Evaluation of Convolutional Neural Network Architectures for Chart Image Classification_", desenvolvido pelos membros do [LABVIS](http://labvis.ufpa.br/) com colaboração do Prof. [Filipe Saraiva](../people/filipe-saraiva), foi aprovado no [IJCNN'2018](http://www.ecomp.poli.br/~wcci2018/). Parabéns aos autores!
