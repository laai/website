---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Mestrado da Antonio Rufino"
---

[Antonio Rufino](../people/antonio-rufino) fará seu exame de qualificação de mestrado em 10 de fevereiro de 2023 às 10:00h na [sala de webconferência do LAAI](https://conferenciaweb.rnp.br/webconf/laai). O título do trabalho é "_Controle de topologia ativa de redes elétricas utilizando o algoritmo Augmented Random Search_".
