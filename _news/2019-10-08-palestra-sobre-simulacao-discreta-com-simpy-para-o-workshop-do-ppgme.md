---
layout: news
icon: "slideshare"
title: "Palestra sobre simulação discreta com SimPy para o Workshop do PPGME"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) realizará a palestra intitulada "Simulação Discreta utilizando SimPy" durante o [I Workshop de Modelagem Matemático-Estatística](https://sites.google.com/view/workshop-ppgme-2019/) do [Programa de Pós-Graduação em Matemática e Estatística da UFPA](http://ppgme.propesp.ufpa.br/). A apresentação será realizada no dia 09/10 às 13h no Auditório do ICEN da UFPA, e os slides estão disponíveis [neste link](https://speakerdeck.com/filipesaraiva/simulacao-discreta-utilizando-simpy).
