---
layout: news
icon: "graduation-cap"
title: "Defesa de TCC de Allan Alcântara"
---

O aluno Allan Alcântara, orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá seu TCC em Bacharelado em Sistemas de Informação no dia 16/12/2019 às 14:30 horas na sala FC-01 da Faculdade de Computação/ICEN. O título do trabalho é "Implementação de uma Interface Web para Gerenciamento de Sistemas Multiagentes do Framework PADE".
