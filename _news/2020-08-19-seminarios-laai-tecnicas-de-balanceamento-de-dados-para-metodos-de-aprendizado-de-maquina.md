---
layout: news
icon: "slideshare"
title: "Seminários LAAI: Técnicas de Balanceamento de Dados para Métodos de Aprendizado de Máquina"
---

Dia 21/08 às 10:00h teremos o início de uma série de seminários a serem apresentados pelos membros do LAAI para divulgar as pesquisas desenvolvidas em inteligência artificial no laboratório. Nesta primeira edição, [Jeanne Pereira](../people/jeanne-pereira) falará sobre técnicas de balanceamento de dados em métodos de aprendizado de máquina, sua importância, funcionamento, e uma aplicação desse tema ao problema de detecção de anomalias em consumidores de energia. O seminário acontecerá na [sala de vídeo conferência](https://conferenciaweb.rnp.br/webconf/laai) do LAAI. Confira nos links o [banner do evento](../img/seminarios-laai-01.jpg) e um [calendário](../calendar/seminarios-laai-01.ical) para salvar em sua agenda.
