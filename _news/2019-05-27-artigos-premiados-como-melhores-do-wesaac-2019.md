---
layout: news
icon: "trophy"
title: "Artigos premiados como melhores do WESAAC'2019"
---

Os 2 artigos submetidos pela equipe do LAAI foram selecionados como os [melhores artigos](https://gsigma.ufsc.br/wesaac2019/index.php/best-papers/) do [WESAAC'2019](https://gsigma.ufsc.br/wesaac2019/)! O artigo "_Mosaik e PADE: Sistemas Multiagentes e Co-Simulação para Modelagem de Redes Elétricas Inteligentes_", em parceria com o [GREI-UFC](https://github.com/grei-ufc/), e o artigo "_Algoritmo Distribuído para Autorrecuperação de Smart Grids Utilizando um Sistema Multiagente Reativo_", de autoria do [Italo Campos](../people/italo-campos) e do Prof. [Filipe Saraiva](../people/filipe-saraiva), serão convidados para publicação em periódicos. Parabéns!
