---
layout: news
icon: "slideshare"
title: "Alunos do LAAI no SEMINIC'2018 na UFPA"
---

Os alunos [Italo Campos](../people/italo-campos/) e Renan Duarte, orientados pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), apresentarão os resultados de suas pesquisas durante o [XXIX Seminário de Iniciação Científica da UFPA](http://seminariopibic.ufpa.br/). A sessão acontecerá dia 02/10/2018 das 10:15 às 10:45 na sala 101 do Mirante do Rio.
