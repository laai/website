---
layout: news
icon: "users"
title: "Nova integrante do LAAI: Jeanne Pereira"
---

[Jeanne Pereira](../people/jeanne-pereira) é a mais nova integrante do LAAI. Aluna de mestrado orientada pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), inicialmente pesquisará aplicações de _deep learning_ a problemas de distribuição de energia elétrica. Bem-vinda!
