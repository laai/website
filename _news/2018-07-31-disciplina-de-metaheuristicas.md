---
layout: news
icon: "pencil-square"
title: "Disciplina de metaheurísticas"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) ministrará a disciplina de Metaheurísticas para Otimização Combinatória durante o período 2018.2 no [PPGCC](http://ppgcc.propesp.ufpa.br/). O horário da disciplina será das 16:40 às 18:20 nas segundas e quartas.
