---
layout: news
icon: "bullhorn"
title: "Chamada de trabalhos para o DARE'2018"
---

Está aberta a chamada de trabalhos para o [_6th International Workshop on Data Analytics for Renewable Energy Integration_](http://dare2018.dnagroup.org/), evento que ocorrerá durante o [ECML/PKDD'2018](http://www.ecmlpkdd2018.org/) em Dublin. O Prof. [Filipe Saraiva](../people/filipe-saraiva) faz parte do comitê de programa do workshop.
