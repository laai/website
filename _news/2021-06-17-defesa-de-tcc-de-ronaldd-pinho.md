---
layout: news
icon: "graduation-cap"
title: "Defesa de TCC de Ronaldd Pinho"
---

O aluno [Ronaldd Pinho](../people/ronaldd-pinho), orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), defenderá seu TCC em Bacharelado em Ciência da Computação no dia 21/06/2021 às 11:00 horas na [sala de webconferência do LAAI](https://conferenciaweb.rnp.br/webconf/laai). O título do trabalho é "_A Comparison of Crossover Operators  in Genetic Algorithms for Switch Allocation Problem in Power Distribution Systems_".
