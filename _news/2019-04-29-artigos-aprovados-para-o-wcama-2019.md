---
layout: news
icon: "file-text"
title: "Artigos aprovados para o WCAMA'2019"
---

O LAAI teve 2 artigos aprovados para o [WCAMA'2019](http://csbc2019.sbc.org.br/eventos/10wcama/): um de autoria do [Marcos Sousa](../people/marcos-sousa) sobre trocas de energia entre uma placa fotovoltaica e a rede de distribuição de energia, e outro do ex-aluno de graduação [Rafael Cavalheiro](../people/rafael-cavalheiro) sobre um medidor de consumo de energia com Arduino. Ambos os artigos foram orientados pelo Prof. [Filipe Saraiva](../people/filipe-saraiva). Parabéns!
