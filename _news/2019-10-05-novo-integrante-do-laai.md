---
layout: news
icon: "users"
title: "Novo Integrante do LAAI"
---

O aluno de graduação Edson Rodrigues, orientado pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), foi recentemente admitido no LAAI para realizar pesquisas com sistemas multiagentes e autorrecuperação em sistemas de distribuição. Bem-vindo ao grupo!
