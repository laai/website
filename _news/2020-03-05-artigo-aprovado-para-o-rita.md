---
layout: news
icon: "file-text"
title: "Artigo aprovado para o RITA"
---

O aluno de mestrado [Italo Campos](../people/italo-campos) teve o artigo intitulado "_Reactive multiagent system applied to self-healing in Smart Grids_" aprovado para publicação no periódico [RITA - Revista de Informática Teórica e Aplicada](https://seer.ufrgs.br/rita/). Essa publicação é motivada pelo artigo ter sido premiado como um dos melhores do WESAAC'2019. Parabéns!
