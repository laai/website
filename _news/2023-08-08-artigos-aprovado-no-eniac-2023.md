---
layout: news
icon: "file-text"
title: "Artigos aprovados no ENIAC'2023"
---

Os alunos de mestrado [Antonio Rufino](../people/antonio-rufino) e [Manoel Garcia](../people/manoel-garcia), orientados pelo prof. [Filipe Saraiva](../people/filipe-saraiva), tiveram artigos aprovados para publicação no [ENIAC'2023](https://www.bracis.dcc.ufmg.br/collocated-events/eniac). Os títulos dos artigos são respectivamente "_Use of Augmented Random Search Algorithm for Transmission Line Control in Smart Grids - A Comparative  Study with RNA-based Algorithms_" e "_Resume Analysis in Portuguese using Word Embeddings: Development of a Decision Support System for Candidate Selection_". Parabéns aos autores!
