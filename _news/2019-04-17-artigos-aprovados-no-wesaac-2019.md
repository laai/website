---
layout: news
icon: "file-text"
title: "Artigos aprovado no WESAAC'2019"
---

A equipe do LAAI teve 2 artigos aprovados para o [WESAAC'2019](https://gsigma.ufsc.br/wesaac2019/): um escrito por [Italo Campos](../people/italo-campos) e o Prof. [Filipe Saraiva](../people/filipe-saraiva) sobre sistemas multiagentes aplicados em recuperação de sistemas elétricos de distribuição, e outro também do Prof. Filipe Saraiva em parceria com os desenvolvedores do [PADE](https://github.com/grei-ufc/pade) sobre a integração desse _framework_ multiagente com a biblioteca de simulação [mosaik](https://mosaik.offis.de/).
