---
layout: news
icon: "users"
title: "Três Novos Integrantes do LAAI"
---

Nos últimos meses o LAAI recebeu 3 novos integrantes para compôr seu time de pesquisa. No mestrado temos [Raimunda Branco](../people/raimunda-branco), que desenvolverá pesquisas sobre utilização de sistemas fuzzy para otimização da produção e uso de energia solar; [Manoel Garcia](../people/manoel-garcia), que também trabalhará com sistemas fuzzy mas para a classificação de aptidões em processos de seleção de recursos humanos; e [Bruno Nishimura](../people/bruno-nishimura), aluno de graduação que pesquisará o uso de sistemas multiagentes para estudar o comportamento de sistemas elétricos frente a inserção de veículos elétricos. Todos os alunos são orientados pelo prof. [Filipe Saraiva](../people/filipe-saraiva). Boas vindas e boas pesquisas!
