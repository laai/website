---
layout: news
icon: "bullhorn"
title: "Edital PPGCC mestrado em ciência da computação 2018"
---

O PPGCC divulgou o [edital 2018](http://www.ppgcc.propesp.ufpa.br/index.php/br/ingresso/selecao-atual) para o mestrado em ciência da computação. O Prof. [Filipe Saraiva](../people/filipe-saraiva) disponibilizou 1 vaga no edital para o [projeto de pesquisa](../research/metodos-metaheuristicos-e-inteligencia-computacional-para-otimizacao-modelagem-e-simulacoes-de-sistemas-eletricos-de-potencia-e-redes-eletricas-inteligentes/) sobre metaheurísticas ou sistemas multiagentes para <i>smart grids</i> em sistemas de distribuição.
