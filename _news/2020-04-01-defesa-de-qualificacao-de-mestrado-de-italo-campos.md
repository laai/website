---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Mestrado da Italo Campos"
---

[Italo Campos](../people/italo-campos) fará o exame de qualificação de mestrado em 6 de abril de 2020 às 10:00h via webconferência. O título do trabalho é "_Aplicação de Sistema Multiagente Reativo para Autorrecuperação de Smart Grids com Balanceamento de Cargas_".
