---
layout: news
icon: "file-text"
title: "Artigos do LAAI aprovados para o IEEE CEC'2020"
---

Os alunos [Jeanne Pereira](../people/jeanne-pereira) e [Ronaldd Pinho](../people/ronaldd-pinho), ambos sob orientação do prof. [Filipe Saraiva](../people/filipe-saraiva), tiveram artigos aprovados para o IEEE CEC'2020, que acontecerá junto ao [WCCI'2020](https://wcci2020.org/) em Glasgow, Reino Unido. Os títulos dos artigos de ambos são, respectivamente, "_A Comparative Analysis of Unbalanced Data Handling Techniques for Machine Learning Algorithms to Electricity Theft Detection_" e "_A Comparison of Crossover Operators in Genetic Algorithms for Switch Allocation Problem in Power Distribution Systems_". Parabéns aos pesquisadores!
