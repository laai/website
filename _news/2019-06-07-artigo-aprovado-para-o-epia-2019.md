---
layout: news
icon: "file-text"
title: "Artigo aprovado para o EPIA'2019"
---

O artigo intitulado "_A Fuzzy System Applied to Photovoltaic Generator Management Aimed to Reduce Electricity Bill_", de autoria do aluno de mestrado [Marcos Sousa](../people/marcos-sousa) sob orientação do Prof. [Filipe Saraiva](../people/filipe-saraiva), foi selecionado para o [EPIA'2019](http://epia2019.utad.pt/) na trilha temática [_Artificial Intelligence in Power and Energy Systems_](https://epia2019.utad.pt/index.php/83-thematic-tracks/100-aipes). Parabéns aos autores!
