---
layout: news
icon: "graduation-cap"
title: "Defesa de TCC de Camil Redwan"
---

O aluno [Camil Redwan](../people/camil-redwan) defenderá seu TCC em Licenciatura em Matemática no dia 19/01/2018 às 11:00 horas no auditório da Faculdade de Matemática. O título do trabalho é "Aplicação de algoritmo genético ao problema de cobertura de vértices".
