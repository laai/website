---
layout: news
icon: "graduation-cap"
title: "Defesa de Qualificação de Mestrado da Ronaldd Pinho"
---

[Ronaldd Pinho](../people/ronaldd-pinho) fará seu exame de qualificação de mestrado em 6 de outubro de 2022 às 9:00h na sala de [webconferência do LAAI](https://conferenciaweb.rnp.br/webconf/laai). O título do trabalho é "_Estudos em Metaheurísticas Populacionais, Construtivas e suas Hibridizações Aplicadas ao Problema do Roteamento de Veículos com Restrição de Capacidade_".
