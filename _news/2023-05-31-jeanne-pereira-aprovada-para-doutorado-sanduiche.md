---
layout: news
icon: "university"
title: "Jeanne Pereira aprovada para Doutorado Sanduíche"
---

[Jeanne Pereira](../people/jeanne-pereira), aluna de doutorado orientada pelo Prof. [Filipe Saraiva](../people/filipe-saraiva), foi aprovada no edital [Programa de Doutorado-Sanduíche no Exterior (PDSE)](https://www.gov.br/capes/pt-br/acesso-a-informacao/acoes-e-programas/bolsas/bolsas-e-auxilios-internacionais/encontre-aqui/paises/multinacional/programa-de-doutorado-sanduiche-no-exterior-pdse) da CAPES e irá realizar parte da sua pesquisa no [Instituto Superior de Engenharia do Porto](https://www.isep.ipp.pt/) em Porto - Portugal, sob orientação da Profa. [Zita Vale](https://scholar.google.com/citations?user=08O4oUkAAAAJ&hl=en). Parabéns Jeanne e bons estudos!
