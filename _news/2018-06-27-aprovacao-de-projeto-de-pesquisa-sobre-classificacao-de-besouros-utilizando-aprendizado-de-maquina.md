---
layout: news
icon: "search"
title: "Aprovação de projeto de pesquisa sobre classificação de besouros utilizando aprendizado de máquina"
---

O Prof. [Filipe Saraiva](../people/filipe-saraiva) teve aprovado na última reunião do [ICEN](http://icen.ufpa.br/) o projeto de pesquisa intitulado "Classificação Visual de Besouros Utilizando Técnicas de Aprendizado de Máquina", para o período 2018-2019. Esse projeto é realizado em parceria com a profa. [Jéssica Viana](http://lattes.cnpq.br/9946346302008600), da UEPA.
