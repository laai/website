---
layout: news
icon: "bullhorn"
title: "Chamada de trabalhos para o CEMiSG'2018"
---

Está aberta a chamada de trabalhos para o [_5th International Workshop on Computational Energy Management in Smart Grids_](http://www.cemisg.org/cemisg2018/), evento que ocorrerá durante o [IEEE WCCI'2018](http://ieee-wcci.org/) no Rio de Janeiro. O Prof. [Filipe Saraiva](../people/filipe-saraiva) faz parte do comitê organizador de ambos os eventos.
