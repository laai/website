---
layout: news
icon: "users"
title: "Jeanne Pereira iniciou doutorado no LAAI"
---

[Jeanne Pereira](../people/jeanne-pereira), que recentemente defendeu dissertação de mestrado, deu continuidade aos estudos iniciando o doutorado sob orientação do Prof. [Filipe Saraiva](../people/filipe-saraiva). Bem-vinda de volta, Jeanne!
