---
layout: news
icon: "file-text"
title: "Artigo aprovado para o WPEIF'2020"
---

O artigo intitulado "Padrões de Projetos para Organizações de Contratos Inteligentes", de autoria do aluno de mestrado [Alan Veloso](../people/alan-veloso) em co-autoria com Billy Anderson, Prof. Antônio Abelém e Prof. [Filipe Saraiva](../people/filipe-saraiva), foi selecionado para o [WPEIF'2020](http://sbrc2020.sbc.org.br/?page_id=1256). Parabéns aos autores!
