---
layout: news
icon: "file-text"
title: "Artigo aprovado para o WBlockhain'2020"
---

O artigo intitulado "Levantamento das Estruturas Organizacionais em Organizações Autônomas Descentralizadas Baseadas em Blockchain", de autoria do aluno de mestrado [Alan Veloso](../people/alan-veloso) em co-autoria com Billy Anderson, Prof. Antônio Abelém e Prof. [Filipe Saraiva](../people/filipe-saraiva), foi selecionado para o [WBlockchain'2020](http://sbrc2020.sbc.org.br/?page_id=1242). Parabéns aos autores!
