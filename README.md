# Laboratory of Applied Artificial Intelligence - Website

[![pipeline status](https://gitlab.com/laai/website/badges/master/pipeline.svg)](https://gitlab.com/laai/website/commits/master)

Este é o repositório do site do
[*Laboratory of Applied Artificial Intelligence*](http://laai.ufpa.br) - LAAI
(Laboratório de Inteligência Artificial Aplicada), um centro de pesquisas
da [Universidade Federal do Pará](http://ufpa.br/) sediado no 
[Instituto de Ciências Exatas e Naturais](http://icen.ufpa.br) e vinculado à
[Faculdade de Computação](http://computacao.ufpa.br/) e ao
[Programa de Pós Graduação em Ciência da Computação](http://ppgcc.propesp.ufpa.br/).

O site é baseado no [Research Group Web Site Template](https://github.com/uwsampa/research-group-web),
um tema para sites desenvolvido em [Jekyll](http://jekyllrb.com/) pelo grupo de
pesquisa [SAMPA](https://sampa.cs.washington.edu/new/index.html), da University
of Washington.

## Editando as Informações do Site

Para editar as informações do site como pessoal, notícias e projetos de
pesquisa, basta editar os arquivos texto correspondentes e em seguida recriar o
site.

### Editando Informações de Pessoal

Para editar informações de pessoal basta editar o arquivo [people.yml](_data/people.yml).

Esse arquivo lista todas as pessoas vinculadas ao LAAI. Basta seguir o modelo
das entradas já existentes e ler as instruções sobre o significado de cada campo
no próprio arquivo.

Uma nota importante diz respeito aos nomes de orientadores (`supervisor`). O
nome deve ser **igual** ao utilizado na entrada do orientador em si nesse
arquivo. Isso é importante para gerar corretamente o link para o perfil do
orientador a partir da página do aluno orientado. A mesma regra vale para o
atributo `research`: deve ser o mesmo valor de algum atributo `name` no arquivo
dos projetos de pesquisa [research.yml](_data/research.yml).

Também é possível adicionar uma imagem para a pessoa na pasta `img`. Essa imagem
será redimensionada para 70x70px e aplicada a um filtro circular, conforme os
exemplos apresentados na página. Recomenda-se colocar uma foto **quadrada** para
evitar distorções. Atenção: a imagem não deve ter mais que 60 KB.

### Editando Informações de Projeto de Pesquisa

As informações de projetos de pesquisa estão em [research.yml](_data/research.yml).

Novamente, basta seguir o modelo presente no próprio arquivo e preencher
corretamente os campos presentes. As explicações de cada campo estão no arquivo.

Uma nota importante para esse arquivo são os nomes dos líderes de projeto
(`leader`) e os estudantes participantes do projeto (`student` - `name`). Como
no preenchimento do cadastro pessoal, os nomes do orientador do projeto e dos
estudantes devem ser **iguais** aos utilizados nos cadastros dos mesmos no
arquivo de pessoal do site. Isso permitirá que os links funcionem corretamente
entre eles após a geração do site.

### Editando Notícias

As notícias estão na pasta [_news/](_news/).

Para o site, escreva notícias curtas por volta de 2 linhas, com no máximo 3
linhas. Privilegiamos pequenos anúncios e divulgação rápida das atividades do
laboratório.

O nome do arquivo deve seguir o formato `YYYY-MM-DD-$título.md`. No arquivo, o
cabeçalho deve obrigatoriamente ter um `title` (igual ao utilizado no nome do
arquivo) e um `icon`.

Para adicionar notícias, basta seguir o modelo utilizado nos demais arquivos.
O campo `title` é utilizado para as páginas de notícias e no feed RSS. O campo
`icon` serve para colocar um ícone na notícia. Algumas sugestões para o campo
`icon` podem ser vistas abaixo, mas você pode pesquisar outros ícones no
[Font Awesome](http://fontawesome.io/cheatsheet/). Quando adicionar na
notícia, apenas remova o prefixo `fa-` na string do campo `icon`.

Algumas sugestões para `icon`:

* "newspaper-o" para notícias
* "file-text" para artigos
* "trophy" para premiação
* "money" para projeto de pesquisa com financiamento
* "code" para software
* "university" para participação em outras universidades
* "users" para notícias sobre adição/remoção de membros
* "graduation-cap" para defesas
* "bullhorn" para avisos de CfP, editais e outros
* "search" para projetos de pesquisa
* "slideshare" para palestras
* "pencil-square" para disciplinas
* e etc. Para mais ícones veja http://fontawesome.io/icons/


Algumas notícias de muito interesse para o site do laboratório:

* Anúncio de abertura de vagas para pesquisadores;
* Anúncio de disciplinas ofertadas pelos membros na pós ou graduação;
* Anúncio de projetos aprovados no âmbito do laboratório;
* Artigos aceitos para publicação;
* Artigos publicados;
* Chamada de trabalhos;
* Defesas dos membros do laboratório;
* Divulgação de eventos;
* Divulgação de processos seletivos;
* Entre outros.

## Como Enviar sua Colaboração

O site do LAAI está hospedado no [Gitlab](https://gitlab.com/laai/website/),
portanto é necessário antes de tudo ter instalado e saber operar o `git`.

Abaixo há algumas maneiras de como enviar uma colaboração.

### Editar via interface web do Gitlab

É possível editar diretamente os arquivos via a própria interface web do Gitlab.
Para tanto, basta ir à página que pretende editar
([people.yml](_data/people.yml), [_news/](_news/) ou
[research.yml](_data/research.yml)) e clicar no botão `Edit` localizado na parte
superior à direita.

Uma tela de edição será aberta e assim é possível alterar o arquivo. Quando
tiver terminado, basta clicar no botão `Commit changes` logo abaixo do editor.
Em seguida, abra um merge request que posteriormente será revisado e inserido
no site.

### Enviar patch

Essa é a maneira mais "manual" de enviar uma colaboração, e ela não requer conta
no Gitlab.

Primeiramente, clone o repositório do website com o seguinte comando:

`$ git clone https://gitlab.com/laai/website.git`

Com o repositório clonado, é possível fazer as alterações necessárias como
cadastrar/editar uma pessoa, um projeto de pesquisa ou uma notícia. Todos esses
arquivos estão na pasta `_data/`.

Após alterar, crie um patch com suas modificações. Se você tiver feito commits
das mudanças, faça um:

`$ git format-patch origin/master`

Se não tiver feito commits ainda, rode:

`$ git diff >> patch`

E envie por e-mail para [saraiva@ufpa.br](mailto:saraiva@ufpa.br) os arquivos
gerados.
