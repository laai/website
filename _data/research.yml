# Dados para serem utilizados para projetos de pesquisa
# - name: Nome do projeto
#   leader: Líder do projeto (normalmente o professor)
#   collaborators: Lista de colaboradores do projeto
#     - name: Nome do colaborador
#       link: Link para alguma página com mais informações do colaborador
#       institution: Instituição ao qual o colaborador é vinculado
#   students: Lista do nome dos estudantes participantes
#     - name: Nome dos estudantes participantes
#   date_begin: Ano de início do projeto
#   date_end: Ano de finalização do projeto
#   summary: Descrição sucinta do projeto
#   description: Descrição completa do projeto
#   funding: Instituições financiam o projeto
#     - sponsor: Nome da instituição
#   link: Link para mais informações como repositório de código ou outro
- name: |
      Análises Comparativas entre Metaheurísticas para Problemas de Otimização Combinatória
  leader: "Filipe Saraiva"
  students:
    - name: "Jeanne Pereira"
  summary: |
    Realização de estudos comparativos entre diferentes metaheurísticas aplicadas a esses problemas clássicos, tentando compreender os diferentes mecanismos de busca empregados e seus impactos na qualidade dos resultados.
  description: |
    Problemas de Otimização Combinatória, em especial os da classe NP-Difícil, são os problemas mais complexos de serem abordados dado que não há algoritmo exato capaz de resolvê-los. Nesse tipo de problema há exemplos clássicos como o Problema do Caixeiro Viajante, Problema da Árvore de Steiner, Problema da Mochila, Problema de Roteamento de Veículos, e muito mais. Cada um desses problemas, para além de suas implicações matemáticas, também tem grande interesse prático pois abstraem problemas do mundo real enfrentados em diversos setores, da logística à engenharia, entre outros. Uma forma possível de encontrar boas soluções a esses problemas está na aplicação de métodos metaheurísticos de otimização, como o Algoritmo Genético, GRASP, Busca Tabu, e outros. Entretanto, cada algoritmo desse utiliza uma estratégia de busca própria, que pode ocasionar em diferentes qualidades nos resultados obtidos. Do exposto, este projeto de pesquisa se propõe a realizar estudos comparativos entre diferentes metaheurísticas aplicadas a esses problemas clássicos, tentando compreender os diferentes mecanismos de busca empregados e seus impactos na qualidade dos resultados.
  date_begin: 2022

- name: |
     Plataformização e Aplicação de Inteligência Artificial para Seleção de Currículos no Sistema Talentos Carreira RH
  leader: "Filipe Saraiva"
  students:
    - name: "Manoel Garcia"
  summary: |
    Projeto aprovado nos editais CNPq 12/2020 e FAPEPI/FINEP/FNDTC 01/2021 em parceria com a empresa Carreira RH para criação de plataforma de RH e utilização de técnicas de inteligência artificial para automação na seleção de currículos.
  description: |
    A Carreira RH detém um sofisticado sistema de gerenciamento de processos seletivos chamado Talentos Carreira RH, que atualmente conta com mais de 20 mil currículos cadastrados e já operou mais de 300 processos para empresas no Piauí, Pará e Maranhão. Entretanto, a maneira como a Carreira RH funciona ainda remete aos tempos pré-plataformas - a empresa é a única responsável por operar os processos de seleção no sistema, vendendo seus serviços de execução e gerenciamento dos processos seletivos, avaliação de candidatos, realização de provas e outros. Em adição à plataformização do sistema, também serão desenvolvidos e aplicados métodos de inteligência artificial para auxiliar na automação e otimização de etapas dos processos seletivos.
  funding:
    - sponsor: CNPq
    - sponsor: FINEP
    - sponsor: FAPEPI
    - sponsor: Carreira RH
  date_begin: 2021

- name: |
     Avaliações entre Metaheurísticas Construtivas, Populacionais, e suas Hibridizações para o Problema de Roteamento de Veículos com Restrição de Capacidade
  leader: "Filipe Saraiva"
  students:
    - name: "Ronaldd Pinho"
  summary: |
    Projeto em parceria com a Loggi sobre estudos comparativos entre diferentes metaheurísticas para o Problema de Roteamento de Veículos com Restrição de Capacidade.
  description: |
    O Problema de Roteamento de Veículos com Restrição de Capacidade é dado por um conjunto de veículos com capacidades de carga limitadas e distintas que devem entregar encomendas a um conjunto de consumidores a custo mínimo. Esse problema modela diferentes situações encontrados na área de logística, transporte de cargas dos mais diferentes tipos, e outros, sendo de grande importância e aplicação prática. Entretanto, por tratar-se de um problema NP-Difícil, abordagens exatas são impraticáveis em grandes instâncias do problema, motivando as aplicações de técnicas metaheurísticas para encontrar boas soluções. Esse projeto intenta realizar a comparação entre técnicas de diferentes famílias de metaheurísticas para avaliação de resultados e verificar possíveis hibridizações: a saber, será uma metaheurística construtiva (GRASP), uma populacional (Algoritmo Genético) e uma hibridização entre elas. Dessa forma, será verificado qual o impacto das diferentes estratégias de busca na qualidade dos resultados obtidos, se alguma técnica se sobresai às demais, e se uma hibridização entre elas consegue juntar o melhor de cada estratégia e encontrar soluções de qualidade superior.
  funding:
    - sponsor: Loggi
  date_begin: 2021

- name: |
    Métodos Metaheurísticos e Inteligência Computacional para Otimização, Modelagem e Simulações de Sistemas Elétricos de Potência e Redes Elétricas Inteligentes
  leader: "Filipe Saraiva"
  students:
    - name: "Camil Redwan"
    - name: "Italo Campos"
    - name: "Jeanne Pereira"
    - name: "Kelly Costa"
    - name: "Marcos Sousa"
    - name: "Rafael Cavalheiro"
    - name: "Renan Duarte"
    - name: "Ronaldd Pinho"
  summary: |
    Aplicação de diferentes técnicas de inteligência computacional para o planejamento e operação de redes elétricas de distribuição e smart grids.
  description: |
    O sistema elétrico de potência, composto pelas etapas de geração, transmissão e distribuição de energia elétrica, é uma das tecnologias de maior impacto no desenvolvimento social e econômico de uma nação, constituindo-se também como um amplo campo de estudos e aplicações de diferentes métodos computacionais voltados à otimização, modelagem e simulações de novas funcionalidades. Além destes, as chamadas "redes elétricas inteligentes" estão engendrando uma nova área de investigação fortemente multidisciplinar onde a computação tem papel fundamental na análise, desenvolvimento e implementação de métodos que integrem comunicação, tratamento de dados, e tomada de decisão sobre funcionalidades destas redes. Com esse cenário em vista, o presente projeto de pesquisa visa criar e estabelecer um grupo multidisciplinar de pesquisadores na Universidade Federal do Pará que tenha foco na utilização de métodos metaheurísticos e inteligência computacional aplicados à funcionalidades tanto dos sistemas elétricos de potência tradicionais quanto das redes elétricas inteligentes, buscando formar mão-de-obra qualificada nesse tipo de aplicação, gerando conhecimento e tecnologia para essa área de pesquisa que tem forte demanda por profissionais com proficiência nas técnicas citadas. Deste projeto derivarão diversos outros relacionados à aplicações de métodos de inteligência computacional para problemas de sistemas elétricos de potência e redes elétricas inteligentes.
  funding:
    - sponsor: UFPA
    - sponsor: CNPq
    - sponsor: CAPES
    - sponsor: FAPESPA
  date_begin: 2016

- name: |
    Classificação Visual de Famílias de Besouros Utilizando Técnicas de Aprendizado de Máquina
  leader: "Filipe Saraiva"
  collaborators:
    - name: "Jéssica Viana"
      link: http://lattes.cnpq.br/9946346302008600
      institution: UEPA
  students:
    - name: "Paulo Sarmento"
  summary: |
    Utilização de técnicas de aprendizado de máquina, incluindo deep learning, para a classificação de besouros de forma a auxiliar o trabalho taxonômico de entomólogos.
  description: |
    Técnicas de aprendizado de máquina permitem, entre outros usos, o processamento de grandes volumas de dados com a finalidade de encontrar padrões entre eles. Uma aplicação potencialmente interessante para a Biologia seria na classificações de grupos com grande número de espécies, como os insetos (Insecta). Neste grupo, a ordem dos besouros (Coleoptera) são um dos que possui maior biodiversidade no planeta, com 28 mil espécies descritas em 105 famílias para só para o Brasil. Devido a essa alta diversidade, a classificação em níveis menores (por exemplo família) muitas vezes é complexa e de difícil realização, em especial para leigos ou pesquisadores de áreas de fora da taxonomia, estando dependente do uso de longas chaves de identificação, do conhecimento da morfologia, experiência com o ato de identificar, e acesso a coleções já anteriormente identificadas para comparação. Diante disso, o projeto pretende implementar diferentes algoritmos para que “aprendam” os padrões presentes nas imagens de certas famílias de besouros, com o objetivo de classificá-las corretamente. Ao final, gostaríamos de entregar um software que auxiliará tanto pesquisadores não taxônomos como o entomólogo taxônomo em besouros nessa tarefa.
  date_begin: 2018

- name: |
   Deep Learning aplicado a Predição de Séries Temporais
  leader: "Lídio Campos"
  students:
    - name: "Yann Figueiredo"
    - name: "Ábner Lucas Alves Pereira"
  summary: |
    O projeto de pesquisa possui como objetivo principal propor modelos computacionais baseados em RNAs diretas profundas e recorrentes (Deep Feedforward, BPTT e LSTM) que serão empregadas em tarefas de predição de séries temporais.
  description: |
    O projeto de pesquisa possui como objetivo principal propor modelos computacionais baseados em RNAs diretas profundas e recorrentes (Deep Feedforward, BPTT e LSTM) que serão empregadas em tarefas de predição de séries temporais, avaliando a performance do Deep Learning em termos de estimação de valores de séries variáveis no tempo em relação às técnicas tradicionais de predição. Além disso, testar diversas estratégias de normalização e balanceamento de dados que possibilitem melhor predição de valores. Avaliar quais variáveis explanatórias são mais importantes para a predição dos dados visando auxiliar o processo de tomada de decisão. Atualmente, estão sendo empregados modelos de predição da tarifa de eletricidade com dados fornecidos pelo Agência Nacional de Energia Elétrica- ANELL para as cinco regiões Brasileiras, onde ainda são poucos os trabalhos sendo desenvolvidos com esse objetivo, visto que a maioria das pesquisas realizadas são de países Europeus. Adicionalmente, serão utilizadas variáveis meteorológicas do banco de dados do sistema de organização nacional de dados ambientais SONDA, estação de Petrolina, do período de 01 de janeiro de 2004 à 31 de março de 2020 e da estação de Macau período de 010.01.2004 a 31.012.2020. O desempenho dos modelos será comparado com 10, 20 e 30 passos para frente.
  funding:
    - sponsor: UFPA
    - sponsor: FAPESPA
  date_begin: 2021

